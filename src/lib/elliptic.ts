import BN from 'bn.js';
import { ec as EC, utils } from 'elliptic';

const TWO_POW_255 = new BN(2).pow(new BN(255))

/**
 * @typedef {object} Curve
 * @property {string} name The number of the curve.
 * @property {BN} p The order of the subgroup G with a generator P, where P is a point specified by the curve.
 * @property {BN} h The cofactor of the subgroup G.
 * @property {string} M SEC1-compressed coordinate of M.
 * @property {string} N SEC1-compressed coordinate of N.
 */
const curveEd25519 = {
  name: 'ed25519',
  // It is defined in [draft-irtf-cfrg-spake2-08] that
  // M: 'd048032c6ea0b6d697ddc2e86bda85a33adac920f1bf18e1b0c6d166a5cecdaf',
  // N: 'd3bfb518f44f3430f29d0c92af503865a1ed3281dc69b35dd868ba85f886c4ab',
  M: new Uint8Array([0xd0, 0x48, 0x03, 0x2c, 0x6e, 0xa0, 0xb6, 0xd6, 0x97, 0xdd, 0xc2, 0xe8, 0x6b, 0xda, 0x85, 0xa3,
    0x3a, 0xda, 0xc9, 0x20, 0xf1, 0xbf, 0x18, 0xe1, 0xb0, 0xc6, 0xd1, 0x66, 0xa5, 0xce, 0xcd, 0xaf]).buffer,
  N: new Uint8Array([0xd3, 0xbf, 0xb5, 0x18, 0xf4, 0x4f, 0x34, 0x30, 0xf2, 0x9d, 0x0c, 0x92, 0xaf, 0x50, 0x38, 0x65,
    0xa1, 0xed, 0x32, 0x81, 0xdc, 0x69, 0xb3, 0x5d, 0xd8, 0x68, 0xba, 0x85, 0xf8, 0x86, 0xc4, 0xab]).buffer,
  p: new BN('7237005577332262213973186563042994240857116359379907606001950938285454250989', 10),
  h: new BN(8)
}

const curveSecp256k1 = {
  name: 'secp256k1',
  // It is defined in [draft-irtf-cfrg-spake2-08] that
  // Compressed
  // M: '02886e2f97ace46e55ba9dd7242579f2993b64e16ef3dcab95afd497333d8fa12f',
  // N: '03d8bbd6c639c62937b04d997f38c3770719c629d7014d49a24b4f98baa1292b49',
  // Uncompressed
  // M: '04886e2f97ace46e55ba9dd7242579f2993b64e16ef3dcab95afd497333d8fa12f5ff355163e43ce224e0b0e65ff02ac8e5c7be09419c785e0ca547d55a12e2d20',
  // N: '04d8bbd6c639c62937b04d997f38c3770719c629d7014d49a24b4f98baa1292b4907d60aa6bfade45008a636337f5168c64d9bd36034808cd564490b1e656edbe7',

  M: new Uint8Array([0x02, 0x88, 0x6e, 0x2f, 0x97, 0xac, 0xe4, 0x6e, 0x55, 0xba, 0x9d, 0xd7, 0x24, 0x25, 0x79, 0xf2,
    0x99, 0x3b, 0x64, 0xe1, 0x6e, 0xf3, 0xdc, 0xab, 0x95, 0xaf, 0xd4, 0x97, 0x33, 0x3d, 0x8f, 0xa1, 0x2f]).buffer,
  N: new Uint8Array([0x03, 0xd8, 0xbb, 0xd6, 0xc6, 0x39, 0xc6, 0x29, 0x37, 0xb0, 0x4d, 0x99, 0x7f, 0x38, 0xc3, 0x77,
    0x07, 0x19, 0xc6, 0x29, 0xd7, 0x01, 0x4d, 0x49, 0xa2, 0x4b, 0x4f, 0x98, 0xba, 0xa1, 0x29, 0x2b, 0x49]).buffer,
  // 2^256 - 2^32 - 2^9 - 2^8 - 2^7 - 2^6 - 2^4 - 1
  p: new BN('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F', 10),
  h: new BN(1),
}

const curveP256 = {
  name: 'p256',
  // It is defined in [draft-irtf-cfrg-spake2-08] that
  // Compressed
  // M: '02886e2f97ace46e55ba9dd7242579f2993b64e16ef3dcab95afd497333d8fa12f',
  // N: '03d8bbd6c639c62937b04d997f38c3770719c629d7014d49a24b4f98baa1292b49',
  // Uncompressed
  // M: '04886e2f97ace46e55ba9dd7242579f2993b64e16ef3dcab95afd497333d8fa12f5ff355163e43ce224e0b0e65ff02ac8e5c7be09419c785e0ca547d55a12e2d20',
  // N: '04d8bbd6c639c62937b04d997f38c3770719c629d7014d49a24b4f98baa1292b4907d60aa6bfade45008a636337f5168c64d9bd36034808cd564490b1e656edbe7',
  M: new Uint8Array([0x02, 0x88, 0x6e, 0x2f, 0x97, 0xac, 0xe4, 0x6e, 0x55, 0xba, 0x9d, 0xd7, 0x24, 0x25, 0x79, 0xf2,
    0x99, 0x3b, 0x64, 0xe1, 0x6e, 0xf3, 0xdc, 0xab, 0x95, 0xaf, 0xd4, 0x97, 0x33, 0x3d, 0x8f, 0xa1, 0x2f]).buffer,
  N: new Uint8Array([0x03, 0xd8, 0xbb, 0xd6, 0xc6, 0x39, 0xc6, 0x29, 0x37, 0xb0, 0x4d, 0x99, 0x7f, 0x38, 0xc3, 0x77,
    0x07, 0x19, 0xc6, 0x29, 0xd7, 0x01, 0x4d, 0x49, 0xa2, 0x4b, 0x4f, 0x98, 0xba, 0xa1, 0x29, 0x2b, 0x49]).buffer,
  // 2^256 - 2^224 + 2^192 + 2^96 - 1
  p: new BN('ffffffff00000001000000000000000000000000ffffffffffffffffffffffff', 10),
  h: new BN(1),
}


/**
 * Enumerate the curves.
 *
 * @readonly
 * @enum {Curve}
 */
export const CURVES = {
  ed25519: curveEd25519,
  secp256k1: curveSecp256k1,
  p256: curveP256,
};

export class Elliptic {
  public name: string;
  public ec;
  public M;
  public N: BN;
  public P: BN;
  public p;
  public h;

  constructor (curve: any) {
    const ec = new EC(curve.name)
    this.name = curve.name
    this.ec = ec.curve
    this.M = this.decodePoint(Buffer.from(curve.M))
    this.N = this.decodePoint(Buffer.from(curve.N))
    this.P = this.ec.g
    this.p = curve.p
    this.h = curve.h
  }

  /**
   * ...
   *
   * @param {Buffer} buf ...
   * @returns {*} ...
   */
  decodePoint (buf: any) {
    if (this.name === 'ed25519') {
      const b = new BN(buf.toString('hex'), 16, 'le')
      // b = [x % 2 (1bit)][y (255bits)]
      return this.ec.pointFromY(b.mod(TWO_POW_255).toString(16), b.gte(TWO_POW_255))
    }
    return this.ec.decodePoint(buf, true)
  }

  /**
   * ...
   *
   * @param {*} p ...
   * @returns {Buffer} ...
   */
  encodePoint (p: any) {
    if (this.name === 'ed25519') {
      const x = p.getX()
      const y = p.getY()
      return Buffer.from(x.mod(new BN(2)).mul(TWO_POW_255).add(y).toArrayLike(Buffer, 'le', 32))
    }
    return Buffer.from(p.encodeCompressed())
  }
}

// module.exports = {
//   CURVES,
//   Elliptic
// }
