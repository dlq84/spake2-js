import BN from 'bn.js';
import * as crypto from 'crypto';

/**
 * Generates a random integer in `[l, r)`.
 *
 * @param {BN} l The lower bound of the random number.
 * @param {BN} r The upper bound of the random number.
 * @returns {BN} A cryptographically-random integer.
 */
export function randomInteger(l: BN, r: BN): BN {
  const range = r.sub(l)
  const size = Math.ceil(range.sub(new BN(1)).toString(16).length / 2)
  const v = new BN(crypto.randomBytes(size + 8).toString('hex'), 16)
  return v.mod(range).add(l)
}
