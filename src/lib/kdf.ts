import hkdf from 'futoin-hkdf';

/**
 * A key derivation function (KDF) based on HMAC with SHA256.
 *
 * @param {Buffer} salt The salt for the HKDF.
 * @param {Buffer} ikm The input key material.
 * @param {Buffer} info The info for the KDF.
 * @returns {Buffer} The derived key.
 */
export function hkdfSha256 (salt: string | Buffer, ikm: Buffer | string, info: Buffer | string): Buffer {
  return hkdf(ikm, 32, { salt, info, hash: 'SHA-256' })
}
