import * as crypto from 'crypto';

/**
 * Computes a key-hashed content with the Secure Hash Algorithm 2 (SHA2 / SHA256) algorithm.
 *
 * @param {Buffer} content The content to be hashed.
 * @param {Buffer} secret The secret key to compute the hash.
 * @returns {Buffer} The key-hashed content.
 */
export function hmacSha256(content: crypto.BinaryLike, secret: crypto.BinaryLike) {
  return crypto.createHmac('sha256', secret).update(content).digest()
}
