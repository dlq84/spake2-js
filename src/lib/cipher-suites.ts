import * as crypto from 'crypto';

import { CURVES, Elliptic } from './elliptic';

import { sha256 } from './hash';
import { hmacSha256 } from './hmac';
import { hkdfSha256 } from './kdf';
import { scrypt, ScryptOptions } from './mhf';

/**
 * @typedef {object} CipherSuite
 * @property {Curve} curve An elliptic curve.
 * @property {Function} hash A hash function.
 * @property {Function} kdf A key derivation function.
 * @property {Function} mac A message authentication code function.
 * @property {Function} mhf A memory-hard hash function.
 */

export interface CipherSuite {
  curve: Elliptic;
  hash: (content: crypto.BinaryLike) => Buffer;
  kdf: (salt: string | Buffer, ikm: Buffer | string, info: Buffer | string) => Buffer;
  mac: (content: crypto.BinaryLike, secret: crypto.BinaryLike) => Buffer;
  mhf: (passphrase: Buffer, salt: Buffer, options: ScryptOptions) => Promise<Buffer>;
}

function suiteEd25519Sha256HkdfHmacScrypt () {
  return <CipherSuite> {
    curve: new Elliptic(CURVES.ed25519),
    hash: sha256,
    kdf: hkdfSha256,
    mac: hmacSha256,
    mhf: scrypt
  };
}


function suiteSecp256k1Sha256HkdfHmacScrypt () {
  return <CipherSuite> {
    curve: new Elliptic(CURVES.secp256k1),
    hash: sha256,
    kdf: hkdfSha256,
    mac: hmacSha256,
    mhf: scrypt
  };
}

function suiteP256Sha256HkdfHmacScrypt () {
  return <CipherSuite> {
    curve: new Elliptic(CURVES.p256),
    hash: sha256,
    kdf: hkdfSha256,
    mac: hmacSha256,
    mhf: scrypt
  };
}

/**
 * Enumerate the cipher suites.
 *
 * @readonly
 * @enum {CipherSuite}
 */
export const cipherSuites: Record<string, () => CipherSuite> = {
  'ED25519-SHA256-HKDF-HMAC-SCRYPT': suiteEd25519Sha256HkdfHmacScrypt,
  'SECP256K1-SHA256-HKDF-HMAC-SCRYPT': suiteSecp256k1Sha256HkdfHmacScrypt,
  'P256-SHA256-HKDF-HMAC-SCRYPT': suiteP256Sha256HkdfHmacScrypt,
}

// exports.cipherSuites = cipherSuites
