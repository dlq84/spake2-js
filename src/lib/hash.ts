import * as crypto from 'crypto';

/**
 * Computes a hashed content with the Secure Hash Algorithm 2 (SHA2 / SHA256) algorithm.
 *
 * @param {Buffer} content The content to be hashed.
 * @returns {Buffer} The hashed content.
 */
export function sha256(content: crypto.BinaryLike) {
  return crypto.createHash('sha256').update(content).digest()
}
