/* global describe, it */
import assert from 'assert';
import { CURVES, Elliptic } from '../../src/lib/elliptic';

describe('lib/elliptic.js', function () {
  describe('Elliptic ed25519', function () {
    it('TODO', async function () {
      const ec = new Elliptic(CURVES.ed25519)
      // Use the internal APIs to check if the points are on the curve
      assert(ec.ec.validate(ec.M))
      assert(ec.ec.validate(ec.N))

      // TODO: encode->decode = self
    })
  })

  describe('Elliptic secp256k1', function () {
    it('TODO', async function () {
      const ec = new Elliptic(CURVES.secp256k1)
      // Use the internal APIs to check if the points are on the curve
      assert(ec.ec.validate(ec.M))
      assert(ec.ec.validate(ec.N))

      // TODO: encode->decode = self
    })
  })

  describe('Elliptic p256', function () {
    it('TODO', async function () {
      const ec = new Elliptic(CURVES.p256)
      // Use the internal APIs to check if the points are on the curve
      assert(ec.ec.validate(ec.M))
      assert(ec.ec.validate(ec.N))

      // TODO: encode->decode = self
    })
  })
})
